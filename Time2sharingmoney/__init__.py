from flask import Flask,request,jsonify,make_response
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import pymysql


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='postgresql://postgres:test123@localhost:5432/Time2sharingmoney'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = 'attapravira'



ma = Marshmallow(app)
db = SQLAlchemy(app)


from Time2sharingmoney import route