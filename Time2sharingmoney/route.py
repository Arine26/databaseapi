from Time2sharingmoney import app,make_response
from flask import request,jsonify
import jwt
import datetime

from Time2sharingmoney.User import User,User_Schema

@app.route('/user',methods=['GET'])
def get_all_user():
    userdata =User.get_all()
    user_Schema=User_Schema(many=True)
    return jsonify(user_Schema.dump(userdata))

@app.route('/signup',methods=['POST'])
def add_User():
    user_data = request.json
    user = User(user_data)
    try:
        user.insert()
        return jsonify(success=True, message='New user add')
    except Exception as e:
        return jsonify(success=False, message=e.args[0])

@app.route('/update/<string:id>',method=['PATCH'])
def update(id):
    user=get_by_id(id)
    user_data = request.json
    if user:
        try:
            user.update(user_data)
            return jsonify(message='user updated successfully')
        except Exception as e:
            return jsonify(message=e.args[0])
    else:
        return jsonify(success=False, message='No User found with id: {}'.format(id)), 404


@app.route('/login')
def login():
    auth =request.authorization
    if not auth or not auth.username or not auth.password:
        return make_response('Could not email', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

    user = User.query.filter_by(Email_id=auth.username).first()

    if not user:
        return make_response('Could not user', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})
    print(user.password)
    # if check_password_hash(user.password, auth.password):
    if user.password == auth.password:
        token = jwt.encode({'public_id' : user.public_id, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])

        return jsonify({'token' : token.decode('UTF-8')})

    return make_response('Could not return', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})



@app.errorhandler(404)
def not_found(error=None):
    message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp
