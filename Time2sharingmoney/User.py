from Time2sharingmoney import db,ma,jsonify
import datetime
import uuid
from passlib.hash import sha256_crypt


class User(db.Model):
    __tablename__='users'
    id = db.Column(db.Integer,primary_key=True)
    public_id= db.Column(db.String(50),unique=True)
    name=db.Column(db.String(20),nullable=False)
    Email_id=db.Column(db.String(50),nullable=False,unique=True)
    password=db.Column(db.String(100),nullable=False)
    phone_no=db.Column(db.String(10),unique=True)
    created_at=db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at=db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __init__(self,user_data):
        self.public_id=str(uuid.uuid4())
        self.name=user_data.get('name')
        self.Email_id=user_data.get('Email_id')
        # self.password= sha256_crypt.encrypt(str(user_data.get('password')))
        self.password=user_data.get('password')
        self.phone_no=user_data.get('phone_no')
        self.created_at=user_data.get('created_at')
        self.updated_at=user_data.get('update_at')

     def update(self, user_data):
        self.name= user_data.get('name', self.name)
        self.Email_id= user_data.get('Email_id', self.Email_id)
        self.password= user_data.get('password', self.password)
        self.phone_no= user_data.get('phone_no', self.phone_no)
        self.updated_at = datetime.datetime.utcnow()
        try:
            #db.session.update()
            db.session.commit()
        except exc.IntegrityError as e:
            db.session.rollback()
            raise Exception('{!r}'.format(e))
        except Exception as e:
            db.session.rollback()
            raise e

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()
        except exc.IntegrityError as e:
            db.session.rollback()
            raise e
        except Exception as e:
            db.session.rollback()
            raise e

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def get_by_id(cls, user_id):
        return cls.query.filter(User.id == user_id).first()



class User_Schema(ma.Schema):
    class Meta:
        fields=('id','name','Email_id','password','phone_no','created_at','updated_at')
